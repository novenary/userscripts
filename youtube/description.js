// ==UserScript==
// @name YouTube - Permanently expand description
// @match *://*.youtube.com/*
// @version 4
// @updateURL https://codeberg.org/novenary/userscripts/raw/branch/trunk/youtube/description.js
// ==/UserScript==

// https://stackoverflow.com/a/61511955
function waitForElm(selector) {
  return new Promise(resolve => {
    if (document.querySelector(selector)) {
      return resolve(document.querySelector(selector));
    }

    const observer = new MutationObserver(() => {
      if (document.querySelector(selector)) {
        resolve(document.querySelector(selector));
        observer.disconnect();
      }
    });

    observer.observe(document.body, {
      childList: true,
      subtree: true
    });
  });
}

window.addEventListener('yt-page-data-updated', () => {
  if (window.location.pathname === '/watch') {
    // Expand the description
    waitForElm('#description-inline-expander > #expand').then(elm => elm.click());
    // Remove the collapse button because it's useless
    waitForElm('#description-inline-expander > #collapse').then(elm => elm.remove());
  }
});
